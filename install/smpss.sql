--招财猪进销存管理软件
-- version 1.1
-- http://www.bojinxiaozhu.com
--
-- 主机: 铂金小猪
-- 生成日期: 2013 年 10 月 04 日 12:05
-- 服务器版本: 5.5.28
-- PHP 版本: 5.2.17

--
-- 数据库: `smpss3`
--

-- --------------------------------------------------------

--
-- 表的结构 `smpss_admin`
--

CREATE TABLE IF NOT EXISTS `smpss_admin` (
  `admin_id` int(10) NOT NULL AUTO_INCREMENT,
  `admin_name` char(16) NOT NULL,
  `admin_pwd` char(32) NOT NULL,
  `gid` int(1) NOT NULL COMMENT '管理用户组ID',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  `lastlogintime` int(10) NOT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='管理员表' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `smpss_admin`
--

INSERT INTO `smpss_admin` (`admin_id`, `admin_name`, `admin_pwd`, `gid`, `createtime`, `lastlogintime`) VALUES
(6, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1373958004, 1380872888);

-- --------------------------------------------------------

--
-- 表的结构 `smpss_category`
--

CREATE TABLE IF NOT EXISTS `smpss_category` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `pid` int(10) NOT NULL COMMENT '父ID',
  `is_show` tinyint(1) NOT NULL COMMENT '是否显示',
  `sort` tinyint(1) NOT NULL COMMENT '排序',
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='商品分类表' AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- 表的结构 `smpss_exlog`
--

CREATE TABLE IF NOT EXISTS `smpss_exlog` (
  `exid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `excredit` float NOT NULL,
  `exrmb` float NOT NULL,
  `goodsid` int(11) DEFAULT NULL,
  `extime` int(11) NOT NULL,
  UNIQUE KEY `exid` (`exid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `smpss_goods`
--

CREATE TABLE IF NOT EXISTS `smpss_goods` (
  `goods_id` int(10) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(100) NOT NULL,
  `goods_sn` varchar(32) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `stock` float NOT NULL DEFAULT '0' COMMENT '库存',
  `warn_stock` tinyint(3) NOT NULL DEFAULT '1' COMMENT '库存警告',
  `weight` varchar(32) NOT NULL,
  `unit` varchar(32) NOT NULL,
  `out_price` decimal(8,2) NOT NULL COMMENT '销售价',
  `in_price` decimal(8,2) NOT NULL COMMENT '进价-未使用',
  `market_price` decimal(8,2) NOT NULL COMMENT '市场价',
  `promote_price` decimal(8,2) NOT NULL COMMENT '促销价',
  `ispromote` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用',
  `promote_start_date` date NOT NULL COMMENT '促销开始时间',
  `promote_end_date` date NOT NULL COMMENT '促销结束时间',
  `ismemberprice` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否享受会员价',
  `creatymd` date NOT NULL COMMENT '商品添加时间',
  `creatdateline` int(10) NOT NULL,
  `lastinymd` date NOT NULL COMMENT '最后进货时间',
  `lastindateline` int(10) NOT NULL,
  `goods_desc` varchar(200) NOT NULL COMMENT '商品简介',
  `countamount` float(10,2) unsigned NOT NULL COMMENT '商品总进价',
  `salesamount` float(10,2) unsigned NOT NULL COMMENT '销售总额',
  PRIMARY KEY (`goods_id`),
  UNIQUE KEY `goods_sn` (`goods_sn`),
  KEY `creatymd` (`creatymd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='商品表' AUTO_INCREMENT=7 ;
-- --------------------------------------------------------

--
-- 表的结构 `smpss_group`
--

CREATE TABLE IF NOT EXISTS `smpss_group` (
  `gid` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(60) NOT NULL,
  `action_code` text NOT NULL COMMENT '权限范围',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0禁用 1可以',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='管理组权限表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `smpss_group`
--

INSERT INTO `smpss_group` (`gid`, `group_name`, `action_code`, `state`) VALUES
(1, '超级管理员', 'a:3:{s:3:"all";i:0;s:6:"action";a:22:{i:0;s:12:"system_index";i:1;s:14:"system_setting";i:2;s:13:"system_rights";i:3;s:16:"system_addrights";i:4;s:10:"system_log";i:5;s:13:"account_index";i:6;s:18:"account_addaccount";i:7;s:17:"account_modifypwd";i:8;s:12:"member_index";i:9;s:16:"member_addmember";i:10;s:12:"member_group";i:11;s:14:"category_index";i:12;s:17:"category_category";i:13;s:11:"goods_index";i:14;s:14:"goods_addgoods";i:15;s:14:"purchase_index";i:16;s:17:"purchase_purchase";i:17;s:11:"sales_index";i:18;s:11:"sales_sales";i:19;s:12:"sales_return";i:20;s:16:"statistics_index";i:21;s:16:"statistics_sales";}s:4:"menu";a:8:{s:6:"system";a:5:{s:5:"index";s:12:"系统信息";s:7:"setting";s:12:"系统配置";s:6:"rights";s:12:"权限管理";s:9:"addrights";s:15:"添加管理组";s:3:"log";s:12:"系统日志";}s:7:"account";a:3:{s:5:"index";s:12:"帐号管理";s:10:"addaccount";s:12:"添加帐号";s:9:"modifypwd";s:12:"密码修改";}s:6:"member";a:3:{s:5:"index";s:12:"帐号管理";s:9:"addmember";s:12:"添加帐号";s:5:"group";s:15:"添加用户组";}s:8:"category";a:2:{s:5:"index";s:12:"分类管理";s:8:"category";s:12:"添加分类";}s:5:"goods";a:2:{s:5:"index";s:12:"商品管理";s:8:"addgoods";s:12:"添加商品";}s:8:"purchase";a:2:{s:5:"index";s:12:"库存管理";s:8:"purchase";s:12:"添加库存";}s:5:"sales";a:3:{s:5:"index";s:12:"销售列表";s:5:"sales";s:12:"商品销售";s:6:"return";s:12:"商品退货";}s:10:"statistics";a:2:{s:5:"index";s:12:"销售统计";s:5:"sales";s:12:"进货统计";}}}', 1),
(2, '普通管理员', 'a:3:{s:3:"all";i:0;s:6:"action";a:4:{i:0;s:12:"system_index";i:1;s:14:"system_setting";i:2;s:13:"system_rights";i:3;s:16:"system_addrights";}s:4:"menu";a:1:{s:6:"system";a:4:{s:5:"index";s:12:"系统信息";s:7:"setting";s:12:"系统配置";s:6:"rights";s:12:"权限管理";s:9:"addrights";s:15:"添加管理组";}}}', 1),
(3, '普通会员', 'a:3:{s:3:"all";i:0;s:6:"action";a:5:{i:0;s:12:"system_index";i:1;s:17:"account_modifypwd";i:2;s:11:"sales_index";i:3;s:11:"sales_sales";i:4;s:12:"sales_return";}s:4:"menu";a:3:{s:6:"system";a:1:{s:5:"index";s:12:"系统信息";}s:7:"account";a:1:{s:9:"modifypwd";s:12:"密码修改";}s:5:"sales";a:3:{s:5:"index";s:12:"销售列表";s:5:"sales";s:12:"商品出库";s:6:"return";s:12:"商品退货";}}}', 1);

-- --------------------------------------------------------

--
-- 表的结构 `smpss_log`
--

CREATE TABLE IF NOT EXISTS `smpss_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL COMMENT '日志类型：0添加商品1商品入库2商品出库',
  `goods_id` int(10) NOT NULL COMMENT '商品ID',
  `content` text NOT NULL,
  `user_id` int(10) NOT NULL,
  `username` varchar(32) NOT NULL,
  `dateymd` date NOT NULL,
  `dateline` int(10) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `dateymd` (`dateymd`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='商品管理日志表' AUTO_INCREMENT=124 ;

-- --------------------------------------------------------

--
-- 表的结构 `smpss_mbgroup`
--

CREATE TABLE IF NOT EXISTS `smpss_mbgroup` (
  `mgid` int(10) NOT NULL AUTO_INCREMENT,
  `mgroup_name` varchar(32) NOT NULL,
  `credit` int(10) NOT NULL COMMENT '消费金额',
  `discount` int(3) NOT NULL COMMENT '折扣',
  PRIMARY KEY (`mgid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='会员用户组' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `smpss_mbgroup`
--

INSERT INTO `smpss_mbgroup` (`mgid`, `mgroup_name`, `credit`, `discount`) VALUES
(1, '普通会员', 0, 100),
(2, '银卡会员', 0, 95),
(3, '金卡会员', 0, 90);

-- --------------------------------------------------------

--
-- 表的结构 `smpss_member`
--

CREATE TABLE IF NOT EXISTS `smpss_member` (
  `mid` int(10) NOT NULL AUTO_INCREMENT,
  `membercardid` varchar(16) NOT NULL COMMENT '会员卡ID',
  `realname` varchar(32) NOT NULL COMMENT '真实名字',
  `phone` varchar(16) NOT NULL COMMENT '座机',
  `mobile` varchar(16) NOT NULL COMMENT '手机',
  `email` varchar(32) NOT NULL COMMENT '邮箱',
  `prov_id` int(10) NOT NULL COMMENT '省份ID',
  `prov_name` varchar(32) NOT NULL COMMENT '省份名称',
  `city_id` int(10) NOT NULL COMMENT '城市ID',
  `city_name` varchar(32) NOT NULL COMMENT '城市名称',
  `address` varchar(200) NOT NULL COMMENT '地址',
  `zipcode` int(7) NOT NULL COMMENT '邮编',
  `cardid` varchar(18) NOT NULL COMMENT '身份证',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态0停用 1可用',
  `grade` tinyint(1) NOT NULL DEFAULT '1' COMMENT '会员等级',
  `credit` int(10) NOT NULL DEFAULT '0' COMMENT '购物积分',
  `regdateymd` date NOT NULL COMMENT '注册时间YMD',
  `regdateline` int(10) NOT NULL COMMENT '注册时间',
  `lastdateline` int(10) NOT NULL COMMENT '最后购物时间',
  PRIMARY KEY (`mid`),
  UNIQUE KEY `membercardid` (`membercardid`),
  KEY `regdateymd` (`regdateymd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='会员信息表' AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- 表的结构 `smpss_member_cache`
--

CREATE TABLE IF NOT EXISTS `smpss_member_cache` (
  `mid` int(11) NOT NULL COMMENT '用户ID',
  `buyfre` int(10) NOT NULL DEFAULT '0' COMMENT '购买次数Frequency',
  `returnfre` int(10) NOT NULL DEFAULT '0' COMMENT '退款次数',
  `exchangecredit` float NOT NULL DEFAULT '0' COMMENT '已兑换积分',
  `totalspanding` float NOT NULL DEFAULT '0' COMMENT '消费总额',
  `totalrefund` float NOT NULL DEFAULT '0' COMMENT '退款总额',
  UNIQUE KEY `mid` (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费信息缓存';


-- --------------------------------------------------------

--
-- 表的结构 `smpss_purchase`
--

CREATE TABLE IF NOT EXISTS `smpss_purchase` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `goods_id` int(10) NOT NULL,
  `goods_sn` varchar(32) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `in_num` float NOT NULL COMMENT '进货数量',
  `out_num` float NOT NULL DEFAULT '0' COMMENT '销售数量',
  `in_price` decimal(8,2) NOT NULL COMMENT '进价',
  `dateymd` date NOT NULL COMMENT '进货时间',
  `dateline` int(10) NOT NULL,
  `isdel` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否可用1不可用 0可用',
  PRIMARY KEY (`id`),
  KEY `goods_sn` (`goods_sn`),
  KEY `dateymd` (`dateymd`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='进货表' AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- 表的结构 `smpss_region`
--

CREATE TABLE IF NOT EXISTS `smpss_region` (
  `region_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `region_name` varchar(120) NOT NULL DEFAULT '',
  `region_ename` varchar(32) NOT NULL COMMENT '地区英文名',
  `level` tinyint(1) NOT NULL COMMENT '地区级别',
  PRIMARY KEY (`region_id`),
  KEY `parent_id` (`parent_id`),
  KEY `region_name` (`region_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户资料中的地区表' AUTO_INCREMENT=6559 ;



-- --------------------------------------------------------

--
-- 表的结构 `smpss_sales`
--

CREATE TABLE IF NOT EXISTS `smpss_sales` (
  `sid` int(10) NOT NULL AUTO_INCREMENT,
  `goods_id` int(10) NOT NULL,
  `goods_sn` varchar(16) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `order_id` varchar(14) NOT NULL,
  `mid` int(10) NOT NULL,
  `membercardid` varchar(16) NOT NULL COMMENT '会员卡卡号',
  `realname` varchar(32) NOT NULL,
  `num` float NOT NULL,
  `price` decimal(8,2) NOT NULL COMMENT '实际售价(优惠后的金额)',
  `out_price` decimal(8,2) NOT NULL COMMENT '商品表的售价(未优惠的价格)',
  `in_price` decimal(8,2) NOT NULL COMMENT '销售时的平均进价',
  `dateymd` date NOT NULL,
  `dateline` int(10) NOT NULL,
  `m_discount` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '会员优惠金额',
  `p_discount` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '促销优惠的金额',
  `refund_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '退款类型 0无退款 1全额退款 2部分退款',
  `refund_num` float NOT NULL DEFAULT '0' COMMENT '退货数量',
  `refund_amount` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '退款金额',
  `sales_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '销售类型 0正常销售1退货销售',
  PRIMARY KEY (`sid`),
  KEY `goods_id` (`goods_id`),
  KEY `goods_sn` (`goods_sn`),
  KEY `order_id` (`order_id`),
  KEY `dateymd` (`dateymd`),
  KEY `membercardid` (`membercardid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='销售记录表' AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- 表的结构 `smpss_system`
--

CREATE TABLE IF NOT EXISTS `smpss_system` (
  `sysname` varchar(100) NOT NULL,
  `options` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- --------------------------------------------------------

--
-- 表的结构 `smpss_tempsales`
--

CREATE TABLE IF NOT EXISTS `smpss_tempsales` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(14) NOT NULL,
  `goods_id` int(10) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `goods_sn` varchar(16) NOT NULL,
  `stock` int(10) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `out_price` float(10,2) unsigned NOT NULL,
  `p_discount` float(10,2) unsigned NOT NULL,
  `ismemberprice` tinyint(1) NOT NULL,
  `ispromote` tinyint(1) NOT NULL,
  `promote_price` float(8,2) unsigned NOT NULL,
  `num` float(10,2) unsigned NOT NULL,
  `dateline` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='销售临时表' AUTO_INCREMENT=45 ;